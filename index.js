/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/


let myInventory = [];
let oppInventory = [];
let brews = [];
let myCast = [];
let oppCast = [];
let myInventoryPlaces = 10;
let tome = [];
let turn = 0;
// game loop
while (true) {
    // Reset
    brews = [];
    myInventory = [];
    oppInventory = [];
    myCast = [];
    oppCast = [];
    myInventoryPlaces = 10;
    tome = [];
    let brewId = 0;
    turn++;
    const actionCount = parseInt(readline()); // the number of spells and recipes in play
    for (let i = 0; i < actionCount; i++) {
        var inputs = readline().split(' ');
        const actionId = parseInt(inputs[0]); // the unique ID of this spell or recipe
        const actionType = inputs[1]; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW

        // Ingredients qui vont être consommés
        const delta0 = parseInt(inputs[2]); // tier-0 ingredient change
        const delta1 = parseInt(inputs[3]); // tier-1 ingredient change
        const delta2 = parseInt(inputs[4]); // tier-2 ingredient change
        const delta3 = parseInt(inputs[5]); // tier-3 ingredient change


        // Nombre de rubis que l'on va récupérer
        let price = parseInt(inputs[6]); // the price in rupees if this is a potion
        const tomeIndex = parseInt(inputs[7]); // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
        const taxCount = parseInt(inputs[8]); // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
        const castable = inputs[9] !== '0'; // in the first league: always 0; later: 1 if this is a castable player spell
        const repeatable = inputs[10] !== '0'; // for the first two leagues: always 0; later: 1 if this is a repeatable player spell

        // Manipulation de mon tableau de commande et de mes sorts
        const item = {
            actionId,
            delta0,
            delta1,
            delta2,
            delta3,
            price,
            actionType,
            tomeIndex,
            taxCount,
            castable,
            repeatable
        };
        if (actionType === 'CAST') {
            const cost = getCastCost(item);
            const itemsAdded = getItemsAdded(item);
            const itemsRemoved = getItemsRemoved(item);
            const nbPlaces = getNumberComponentGenerated(item);
            myCast.push({...item, cost, nbPlaces, itemsAdded, itemsRemoved});
        } else if (actionType === 'OPPONENT_CAST') {
            const cost = getCastCost(item);
            const itemsAdded = getItemsAdded(item);
            const itemsRemoved = getItemsRemoved(item);
            const nbPlaces = getNumberComponentGenerated(item);
            oppCast.push({...item, nbPlaces, cost, itemsAdded, itemsRemoved})
        } else if (actionType === 'BREW') {
            if (brewId === 0) {
                item.price += 3;
            }

            if(brewId === 1) {
                item.price += 1;
            }

            brewId++;
            brews.push({...item});
        } else if (actionType === 'LEARN') {
            const cost = getLearnCost(item);
            const itemsAdded = getItemsAdded(item);
            const itemsRemoved = getItemsRemoved(item);
            const nbPlaces = getNumberComponentGenerated(item);
            tome.push({...item, cost, itemsAdded, itemsRemoved, nbPlaces});
        }
    }
    for (let i = 0; i < 2; i++) {
        var inputs = readline().split(' ');
        const inv0 = parseInt(inputs[0]); // tier-0 ingredients in inventory
        const inv1 = parseInt(inputs[1]);
        const inv2 = parseInt(inputs[2]);
        const inv3 = parseInt(inputs[3]);
        const score = parseInt(inputs[4]); // amount of rupees

        // Moi
        if (i === 0) {
            myInventoryPlaces = 10 - inv0 - inv1 - inv2 - inv3;
            myInventory[0] = inv0;
            myInventory[1] = inv1;
            myInventory[2] = inv2;
            myInventory[3] = inv3;
        } else {
            // l'autre
            oppInventory[0] = inv0;
            oppInventory[1] = inv1;
            oppInventory[2] = inv2;
            oppInventory[3] = inv3;
        }

    }


    //Algo
    let targetBrews = sortBrews(brews);
    targetBrews = targetBrews.slice(0, 3);

    let itemsNeeded = getItemsNeeded([], myCast, targetBrews);
    let reducedBrews = reduceBrews(brews, myInventory);
    let reducedCasts = reduceCasts(myCast, myInventory, itemsNeeded);
    let reducedLearns = reduceLearns(tome, myInventory, itemsNeeded);
    let usefulUncastableCasts = myCast.filter(item => castIsUseful(item, itemsNeeded) && !item.castable);
    let learnsNullCost = [];

    tome.forEach(item => {
        item.score = getLearnScore(itemsNeeded, item);
    });

    reducedCasts.forEach(item => {
        item.score = getLearnScore(itemsNeeded, item);
    });
    reducedCasts = sortLearnsByCost(reducedCasts);
    reducedCasts = sortLearnsByScore(reducedCasts);

    reducedBrews = sortBrews(reducedBrews);

    reducedLearns = sortLearnsByCost(reducedLearns);
    reducedLearns = sortLearnsByScore(reducedLearns);

    learnsNullCost = getLearnNullCost(reducedLearns);
    learnsNullCost = sortLearnsByTomeIndex(learnsNullCost);
    learnsNullCost = sortLearnsByScore(learnsNullCost);

    console.warn('itemsNeeded', itemsNeeded, usefulUncastableCasts);


    // 1er lance fabrication commande
    // 2eme lance sort
    // 3eme recharge les sorts
    if (reducedBrews.length > 0) {
        console.warn('brew', brews[0]);
        console.log(`BREW ${reducedBrews[0].actionId}`, 'BREW');
    } else if (learnsNullCost.length) {
        console.warn('learn 0', learnsNullCost[0]);
        console.log(`LEARN ${learnsNullCost[0].actionId}`, 'LEARN0');
    } else if (reducedCasts.length > 0) {
        console.warn('cast', reducedCasts[0], reducedCasts[1], reducedCasts[2]);
        console.log(`CAST ${reducedCasts[0].actionId} ${reducedCasts[0].repeatable ? 1 : ''}`, 'CAST');
    } else if (reducedLearns.length > 0 && !halfCastAreUsed() && !usefulUncastableCasts.length > 1) {
        console.warn('learn', reducedLearns[0], reducedLearns.length > 0 , !halfCastAreUsed() , usefulUncastableCasts.length > 1);
        console.log(`LEARN ${reducedLearns[0].actionId}`, 'LEARN')
    } else {
        console.warn('rest',reducedLearns[0], allCastAreUsed(), halfCastAreUsed());
        console.log('REST', 'REST');
    }

    // Write an action using console.log()
    // To debug: console.error('Debug messages...');


    // in the first league: BREW <id> | WAIT; later: BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT

}


// TODO : BREWS

function reduceBrews(brews, inventory) {
    const reducedBrews = [];
    brews.forEach(brew => {
        if (brewCanBeMake(brew, inventory)) {
            reducedBrews.push(brew);
        }
    })
    return reducedBrews;
}

function brewCanBeMake(potion, inventory) {
    if (
        Math.abs(potion.delta0) <= inventory[0] &&
        Math.abs(potion.delta1) <= inventory[1] &&
        Math.abs(potion.delta2) <= inventory[2] &&
        Math.abs(potion.delta3) <= inventory[3]
    ) {
        return true;
    }
    return false;
}

function sortBrews(brews) {
    return brews.sort((a, b) => {
        if (a.price < b.price) {
            return 1;
        }
        if (a.price > b.price) {
            return -1;
        }
        return 0;
    })
}

// TODO : CASTS

function reduceCasts(casts, inventory, itemsNeeded) {
    const reducedCasts = [];
    casts.forEach(cast => {
        if (cast.castable && havePlace(cast) && castIsUseful(cast, itemsNeeded) && castCanBeMake(cast, inventory)) {
            reducedCasts.push(cast);
        }
    });
    return reducedCasts;
}

function castIsUseful(cast, itemsNeeded) {

    return (itemsNeeded[0] > 0 && cast.itemsAdded[0] > 0) ||
        (itemsNeeded[1] > 0 && cast.itemsAdded[1] > 0) ||
        (itemsNeeded[2] > 0 && cast.itemsAdded[2] > 0) ||
        (itemsNeeded[3] > 0 && cast.itemsAdded[3] > 0);
}

function castCanBeMake(cast, inventory) {
    const delta0 = cast.delta0 < 0 ? cast.delta0 : 0;
    const delta1 = cast.delta1 < 0 ? cast.delta1 : 0;
    const delta2 = cast.delta2 < 0 ? cast.delta2 : 0;
    const delta3 = cast.delta3 < 0 ? cast.delta3 : 0;
    if (
        Math.abs(delta0) <= inventory[0] &&
        Math.abs(delta1) <= inventory[1] &&
        Math.abs(delta2) <= inventory[2] &&
        Math.abs(delta3) <= inventory[3]
    ) {
        return true;
    }
    return false;
}

function getCastCost(cast) {
    return (cast.delta0 < 0 ? Math.abs(cast.delta0) : 0) + (cast.delta1 < 0 ? Math.abs(cast.delta1) : 0) + (cast.delta2 < 0 ? Math.abs(cast.delta2) : 0) + (cast.delta3 < 0 ? Math.abs(cast.delta3) : 0);
}

function getNumberComponentGenerated(cast) {
    return (cast.delta0 > 0 ? cast.delta0 : 0) + (cast.delta1 > 0 ? cast.delta1 : 0) + (cast.delta2 > 0 ? cast.delta2 : 0) + (cast.delta3 > 0 ? cast.delta3 : 0);
}

function havePlace(cast) {
    return myInventoryPlaces - cast.nbPlaces + cast.cost > 0;
}

function getItemsAdded(cast) {
    const items = [0, 0, 0, 0];
    if (cast.delta0 > 0) {
        items[0] = cast.delta0;
    }
    if (cast.delta1 > 0) {
        items[1] = cast.delta1;
    }
    if (cast.delta2 > 0) {
        items[2] = cast.delta2;
    }
    if (cast.delta3 > 0) {
        items[3] = cast.delta3;
    }
    return items;
}

function getItemsRemoved(cast) {
    const items = [0, 0, 0, 0];
    if (cast.delta0 < 0) {
        items[0] = cast.delta0;
    }
    if (cast.delta1 < 0) {
        items[1] = cast.delta1;
    }
    if (cast.delta2 < 0) {
        items[2] = cast.delta2;
    }
    if (cast.delta3 < 0) {
        items[3] = cast.delta3;
    }
    return items;
}

function getMaxRepeatCast(cast, inventory) {
    let nbAdded = cast.itemsAdded.reduce((accumulator, currentValue) => accumulator + currentValue);
    let nbRemoved = cast.itemsRemoved.reduce((accumulator, currentValue) => accumulator + currentValue);

    if ((nbAdded - nbRemoved) > 0) {
        return Math.trunc(myInventoryPlaces / (nbAdded - nbRemoved));
    }
    return 1;
    // const nbItem0 = cast.delta0 !== 0 && cast.delta0 < 0 ? (inventory[0] /  cast.delta0) : 10;
    // const nbItem1 = cast.delta1 !== 0 && cast.delta1 < 0 ? (inventory[1] /  cast.delta1) : 10;
    // const nbItem2 = cast.delta2 !== 0 && cast.delta2 < 0 ? (inventory[2] /  cast.delta2) : 10;
    // const nbItem3 = cast.delta3 !== 0 && cast.delta3 < 0 ? (inventory[3] /  cast.delta3) : 10;
    // return Math.min(nbItem0, nbItem1, nbItem2, nbItem3);
}

function allCastAreUsed() {
    let areUsed = true;
    myCast.forEach(cast => {
        if (cast.castable) {
            areUsed = false;
        }
    })
    return areUsed;
}

function halfCastAreUsed() {
    let cptUsed = 0;
    myCast.forEach(cast => {
        if (cast.castable) {
            cptUsed++;
        }
    })
    if ((myCast.length / (cptUsed !== 0 ? cptUsed : 1)) < 2 ) return true;
    return false;
}

// TODO : LEARN

function getLearnScore(itemsNeeded, learn) {
    let score = 0;
    let itemsAdded = learn.itemsAdded;
    if (itemsNeeded[0] > 0 && itemsAdded[0] > 0) {
        score += itemsAdded[0];
    }
    if (itemsNeeded[1] > 0 && itemsAdded[1] > 0) {
        score += itemsAdded[1];
    }
    if (itemsNeeded[2] > 0 && itemsAdded[2] > 0) {
        score += itemsAdded[2];
    }
    if (itemsNeeded[3] > 0 && itemsAdded[3] > 0) {
        score += itemsAdded[3];
    }
    return score;
}

function getLearnCost(learn) {
    return getCastCost(learn);
}

function getLearnNullCost(learns) {
    return learns.filter(learn => learn.cost === 0 && (learn.taxCount - learn.tomeIndex) < myInventoryPlaces);
}

function reduceLearns(learns, inventory, itemsNeeded) {
    const reducedLearns = [];
    learns.forEach(learn => {
        if (learn.tomeIndex <= inventory[0] && (learn.taxCount - learn.tomeIndex) < myInventoryPlaces && castIsUseful(learn, itemsNeeded)) {
            reducedLearns.push(learn);
        }
    });
    return reducedLearns;
}

function sortLearnsByTomeIndex(learns) {
    return learns.sort((a, b) => {
        if (a.tomeIndex < b.tomeIndex) {
            return -1;
        }
        if (a.tomeIndex > b.tomeIndex) {
            return 1;
        }
        return 0;
    })
}

function sortLearnsByCost(learns) {
    return learns.sort((a, b) => {
        if (a.cost < b.cost) {
            return -1;
        }
        if (a.cost > b.cost) {
            return 1;
        }
        return 0;
    })
}

function sortLearnsByScore(learns) {
    return learns.sort((a, b) => {
        if (a.score > b.score) {
            return -1;
        }
        if (a.score < b.score) {
            return 1;
        }
        return 0;
    })
}

// TODO ITEMS
function getItemsNeeded(learns, casts, brews) {
    let items = [0, 0, 0, 0];

    // Add for learns
    learns.forEach(learn => {
        items[0] = Math.max(items[0], learn.delta0 < 0 ? Math.abs(learn.delta0) : 0);
        items[1] = Math.max(items[1], learn.delta1 < 0 ? Math.abs(learn.delta1) : 0);
        items[2] = Math.max(items[2], learn.delta2 < 0 ? Math.abs(learn.delta2) : 0);
        items[3] = Math.max(items[3], learn.delta3 < 0 ? Math.abs(learn.delta3) : 0);
    });

    // Add for casts
    casts.forEach(cast => {
        items[0] = Math.max(items[0], cast.delta0 < 0 ? Math.abs(cast.delta0) : 0);
        items[1] = Math.max(items[1], cast.delta1 < 0 ? Math.abs(cast.delta1) : 0);
        items[2] = Math.max(items[2], cast.delta2 < 0 ? Math.abs(cast.delta2) : 0);
        items[3] = Math.max(items[3], cast.delta3 < 0 ? Math.abs(cast.delta3) : 0);
    });

    // Add for brews
    brews.forEach(brew => {
        items[0] = Math.max(items[0], brew.delta0 < 0 ? Math.abs(brew.delta0) : 0) * 2;
        items[1] = Math.max(items[1], brew.delta1 < 0 ? Math.abs(brew.delta1) : 0) * 2;
        items[2] = Math.max(items[2], brew.delta2 < 0 ? Math.abs(brew.delta2) : 0) * 2;
        items[3] = Math.max(items[3], brew.delta3 < 0 ? Math.abs(brew.delta3) : 0) * 2;
    });

    items[0] -= myInventory[0];
    items[1] -= myInventory[1];
    items[2] -= myInventory[2];
    items[3] -= myInventory[3];

    return items;
}

// TODO UTILS
function deltaToIndex(delta) {
    switch (delta) {
        case 'delta0':
            return 0;
        case 'delta1':
            return 1;
        case 'delta2':
            return 2;
        case 'delta3':
            return 3;
        default:
            return null;
    }

}

function indexToDelta(index) {
    switch (index) {
        case 0:
            return "delta0";
        case 1:
            return "delta1";
        case 2:
            return "delta2";
        case 3:
            return "delta3";
        default:
            return null;
    }
}

function getTotalInInventory(itemIndex) {
    return myInventory[itemIndex];
}
